#####################################
# MAINTAINER - GINU MATHEW          #
# Custom VPC, EC2, ELB , AUTOSCALE  #
#####################################

######## CUSTOM VPC CREATION ##########

resource "aws_vpc" "ginuvpc" {
  cidr_block       = "172.168.0.0/16"
  instance_tenancy = "default"
  enable_dns_support = "true"
  enable_dns_hostnames = "true"

  tags = {
    Name = "ginuvpc"
  }
}


#######################################################
# Public subnet 1 & 2 are in same AZ =  eu-central-1a #
# Private subnet 3 in Az = eu-central-1b              #
#######################################################

##### PUBLIC SUBNET - 1 ####

resource "aws_subnet" "publicsub1" {   
  vpc_id     = aws_vpc.ginuvpc.id
  availability_zone = "eu-central-1a"
  cidr_block = "172.168.0.0/18"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "Publicsub1"
  }
}

##### PUBLIC SUBNET - 2 #####

resource "aws_subnet" "publicsub2" {
  vpc_id     = aws_vpc.ginuvpc.id
    availability_zone = "eu-central-1a"
  cidr_block = "172.168.64.0/18"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "Publicsub2"
  }
}


##### PUBLIC SUBNET - 3 #####

resource "aws_subnet" "publicsub3" {
  vpc_id     = aws_vpc.ginuvpc.id
    availability_zone = "eu-central-1b"
  cidr_block = "172.168.192.0/18"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "Publicsub3"
  }
}


##### PRIVATE SUBNET #####

resource "aws_subnet" "Privatesub" {
  vpc_id     = aws_vpc.ginuvpc.id
  availability_zone = "eu-central-1b"
  cidr_block = "172.168.128.0/18"

  tags = {
    Name = "Privatesub"
  }
}

#### INTERNET GATEWAY ####

resource "aws_internet_gateway" "ginuig" {
  vpc_id = aws_vpc.ginuvpc.id

  tags = {
    Name = "ginuig"
  }
}


#### ELASTIC IP FOR NAT GATEWAY #####

resource "aws_eip" "ginueip" {
  vpc      = true
}


#### NAT GATEWAY ####

resource "aws_nat_gateway" "ginunatgw" {
  allocation_id = aws_eip.ginueip.id
  subnet_id     = aws_subnet.publicsub2.id

  tags = {
    Name = "ginunatgw"
  }
}



#### ROUTE TABLE FOR PUBLIC 1 & 2 & 3 ####

resource "aws_route_table" "ginurt1" {
  vpc_id = aws_vpc.ginuvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ginuig.id
  }

  tags = {
  
  Name = "PublicRoute"
  }
  }

#### ROUTE TABLE FOR PRIVATE  ####

resource "aws_route_table" "ginurt2" {
  vpc_id = aws_vpc.ginuvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ginunatgw.id
  }
  tags = {
  
  Name = "PrivateRoute"
  }
  }

#### ROUTE TABLE ASSOCIATION FOR PUBLIC 1 & 2 & 3 ####


resource "aws_route_table_association" "ginurta1" {
  subnet_id      = aws_subnet.publicsub2.id
  route_table_id = aws_route_table.ginurt1.id
  }

resource "aws_route_table_association" "ginurta2" {
  subnet_id      = aws_subnet.publicsub1.id
  route_table_id = aws_route_table.ginurt1.id
  }

  resource "aws_route_table_association" "ginurta4" {
  subnet_id      = aws_subnet.publicsub3.id
  route_table_id = aws_route_table.ginurt1.id
  }

#### ROUTE TABLE ASSOCIATION FOR PRIVATE ####

resource "aws_route_table_association" "ginurta3" {
  subnet_id      = aws_subnet.Privatesub.id
  route_table_id = aws_route_table.ginurt2.id
}



#### SECURITY GROUP FOR BASTION SERVER  ####

resource "aws_security_group" "ginussh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.ginuvpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_SSH"
  }
}


#### SECURITY GROUP FOR Autoscaling Server- ALLOW ACCESS FROM BASTION SERVER  ONLY ####

resource "aws_security_group" "ginuserver2ssh" {
  name        = "allow_SSH_pub2"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.ginuvpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups  =  [ aws_security_group.ginussh.id ]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   
   }
    
    ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_SSH_from_pub1"
  }
}


#### SECURITY GROUP FOR LOAD BALANCER SERVER ####

resource "aws_security_group" "ginuelbserver" {
  name        = "allow_HTTP_ELB_SERVER"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.ginuvpc.id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   
   }
    

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_HTTP_HTTPS_ELB"
  }
}




#### AWS KEY PAIR CREATION ####

resource "aws_key_pair" "ginuawskey" {
  key_name   = "ginuawskey"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCdCPhhE8yAJxrmas+WLWj/H84HkKzbst63SVwNHfcMQdGsn14oiH4cVRz0jJBARc+Z7lOR10GbVqI5XWNBpxOSEj9/A6544WhIRR92juXAM+k3CFnKN7qZ9iwTAaGPxcPXnpt2gfMsDL5E/zm7Hk2ay64sbbZ/bgnyCdH64fFC36XxgYxCoh7m3jBmHaCaPW0nibZg9B3XBUgxKGIZIfab8Sd8CqqnH9ZgogwZanikF4UfvMglJ3cLO2IBN2yEqL69+4l1KPjvGtnsCoPSncp0Q10EB8398EThehcMyg6T9XURIa0k0QfUBjJV9oU0ezSeNyMXQ8y7c42VirxlnWwAKkem7ojE2efzjhroFF+aD+E2Zr9r93Rtm2YSl8DoW5sQK8z43nMo5dkROIIFlEGh2F53AqgPywOAdzvTr3raEeyvSRw2MvdrlxNaAd1mnbAcaotCzuA2BdM3hiJig89Ymo8iHr0CFmjSjozgSrujHwc2goEzh/PeN7fBfLcRHjc= root@ip-172-31-7-20"
}


#### LAUNCH CONFIGURATION - SSH ACCESS ONLY FROM BASTION SERVER ####

resource "aws_launch_configuration" "ginulc" {
  
  name_prefix   = "ginulaunchconfig"
  image_id      = "ami-0502e817a62226e03"
  security_groups   = [ aws_security_group.ginuserver2ssh.id ]
  instance_type = "t3.micro"
  key_name    =  aws_key_pair.ginuawskey.id
  user_data = file ("launch.sh")

  lifecycle {
    create_before_destroy = true
  }
}



#### BASTION SERVER ####


resource "aws_instance" "bastionserver" {
  ami           =  "ami-0502e817a62226e03"
  instance_type = "t3.micro"
  key_name      = aws_key_pair.ginuawskey.id
  subnet_id =  aws_subnet.publicsub1.id
  vpc_security_group_ids = [ aws_security_group.ginussh.id ]
  associate_public_ip_address = "true"

  tags = {
    Name = "BASTION SERVER"
  }
}




#### AUTO SCALING GROUPS - MIN 2 - DESIRED 2 - MAX 4 ####


resource "aws_autoscaling_group"  "ginuag" {

  launch_configuration = aws_launch_configuration.ginulc.id
  min_size = 2
  max_size = 4
  desired_capacity  = 2
  health_check_type = "ELB"
  load_balancers = [ aws_elb.ginuelb.id ] 
  vpc_zone_identifier = [ aws_subnet.publicsub1.id, aws_subnet.publicsub2.id ]
 
 lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "ginu-autoscaling"
    propagate_at_launch = true
  }
}



####  LOAD BALANCER  - CLASSIC LB ####

resource "aws_elb" "ginuelb" {
  name = "terraform-asg-example"
  security_groups = [ aws_security_group.ginuelbserver.id ]
  subnets = [ aws_subnet.publicsub3.id, aws_subnet.publicsub2.id ] 
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 10
    target = "HTTP:80/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }
}

